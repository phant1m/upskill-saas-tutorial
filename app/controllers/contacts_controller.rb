class ContactsController < ApplicationController
    
    #GET request to /contact-us
    #Show new contact form
    def new
        @contact = Contact.new
    end
    
    #POST request to /contacts
    def create
            #Mass assginment of form fileds into contact objects.
            @contact = Contact.new(contact_params)
            #Save the form object to the database.
            if @contact.save
                #Store form fields via parameters, into variables.
                name = params[:contact][:name]
                email = params[:contact][:email]
                body = params[:contact][:comments]
                #Plug variables into Contact Mailer.
                #email method and send Email.
                ContactMailer.contact_email(name, email, body).deliver
                #Store success message in flash hash
                flash[:success] = "Message on the way somewhere."
            else
                #If Contact object doesnt save, store errors in flash hash,
                flash[:danger] = @contact.errors.full_messages.join(", ")
            end
            redirect_to new_contact_path
    end
    private
    #To collect data from form we need to use strong parameters
    #and whitelist the form fields.
      def contact_params
         params.require(:contact).permit(:name, :email, :comments)
      end
end